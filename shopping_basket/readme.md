## Overview
A shopping basket pricing calculator, for the sole purpose of demonstrating 
a capability to, given a problem statement, build a simple, extensible system 
that is relatively well designed, and relatively well tested.

## Running
As this would be, essentially, a library-based module, there is no capability to
run the application as part of the demonstration. Instead, for verification and 
critique, one can run the tests, of which there are many (~85.. of course, this
is not particularly exhaustive). (A simple app.py file
would suffice to "fiddle" around with the code outside of testing.)

## Testing
To run the tests, one must have installed, as per the Pipfile, pytest,
and, assuming one is within the <b>shopping_basket</b> directory, one 
can simply run the following command:

```
    pytest -vv
```

## Thoughts
The implementation feels fairly weak, perhaps mostly due to the lack of actual 
possible items within the catalogue, the shopping basket, and the lack of 
discount calculators.

However, this was a conscious decision: I wished to display slightly more elegance
with regard to the general system-level design, including the illumination of OOP
concepts, as there was <i>frequent</i> mention of this being used as part of larger systems.
(I think this is so very trivial to extend, that given another hour, there could be a half
dozen more items and discount calculators.)

Aside from this and due to time constraints whereby I didn't want to allocate more
time that what was advised (this took almost exactly 3 hours), the testing isn't
exhaustive and as a consequence there are likely edge cases that are to cause problems.

What I would do in the real world, is likely have the catalogue and discounts within
a configuration, such as json, and load them into items either on an event trigger or 
as per a pre-defined schedule, but I ran out of time.

I do hope that time considerations are taken into account, and I very much look forward
to critical feedback and a potential discussion regarding both this and future work.

:)
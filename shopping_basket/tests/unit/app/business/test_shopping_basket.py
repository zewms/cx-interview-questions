import pytest

from app.business.implemetations.shopping_basket import ShoppingBasket

_SHOPPING_BASKET = ShoppingBasket()


def test_shopping_basket_items_is_none() -> None:
    _SHOPPING_BASKET.clear()

    assert _SHOPPING_BASKET.items() == []


@pytest.mark.parametrize(
    "item_name",
    [
        None,
        "",
        "    "
    ]
)
def test_shopping_basket_add_item_name_is_invalid(
        item_name: str
) -> None:
    _SHOPPING_BASKET.clear()

    with pytest.raises(ValueError) as _:
        assert _SHOPPING_BASKET.add(
            item_name=item_name
        )


@pytest.mark.parametrize(
    "item_name",
    [
        "Test Name #1",
        "Test Name #2",
        "Test Name #3"
    ]
)
def test_shopping_basket_remove_item_is_absent(
        item_name: str
) -> None:
    _SHOPPING_BASKET.clear()

    with pytest.raises(ValueError) as _:
        _ = _SHOPPING_BASKET.remove(
            item_name=item_name
        )


@pytest.mark.parametrize(
    "item_name,item_count",
    [
        ("Test Name #1", 1),
        ("Test Name #2", 3),
        ("Test Name #XYZ", 13)
    ]
)
def test_shopping_basket_add_item(
        item_name: str,
        item_count: int
) -> None:
    _SHOPPING_BASKET.clear()

    [
        _SHOPPING_BASKET.add(
            item_name=item_name
        )
        for _ in range(item_count)
    ]

    assert _SHOPPING_BASKET.items() == [(item_name, item_count)]


@pytest.mark.parametrize(
    "item_name,item_count",
    [
        ("Test Name #1", 1),
        ("Test Name #2", 5),
        ("Test Name #XYZ", 3)
    ]
)
def test_shopping_basket_remove_item(
        item_name: str,
        item_count: int
) -> None:
    _SHOPPING_BASKET.clear()

    [
        _SHOPPING_BASKET.add(
            item_name=item_name
        )
        for _ in range(item_count)
    ]

    removed = _SHOPPING_BASKET.remove(
        item_name=item_name
    )

    assert removed == (item_name, item_count)
    assert not any(_SHOPPING_BASKET.items())

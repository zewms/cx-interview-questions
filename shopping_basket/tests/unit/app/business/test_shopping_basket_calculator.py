import pytest

from app.business.implemetations.shopping_basket import ShoppingBasket
from app.business.implemetations.shopping_basket_calculator import \
    ShoppingBasketCalculator
from app.models.catalogue import Catalogue
from app.models.receipt import Receipt
from app.models.shopping_basket_calculator_request import \
    ShoppingBasketCalculatorRequest
from app.models.special_offers import SpecialOffers
from tests.mock_data import catalogues, special_offers

_SHOPPING_BASKET_CALCULATOR = ShoppingBasketCalculator()


def test_shopping_basket_calculator_basket_items_zero() -> None:
    assert _SHOPPING_BASKET_CALCULATOR.calculate(
        ShoppingBasketCalculatorRequest(
            basket=ShoppingBasket(),
            catalogue=catalogues.CATALOGUE_FULL,
            special_offers=special_offers.SPECIAL_OFFERS_FULL
        )
    ) == Receipt(
        sub_total=0.00,
        discount=0.00,
        total=0.00
    )


@pytest.mark.parametrize(
    "item_name",
    [
        "Test Name #1",
        "Test Name #2",
        "Test Name #3"
    ]
)
def test_shopping_basket_calculator_basket_item_non_existent(
        item_name: str
) -> None:
    assert _SHOPPING_BASKET_CALCULATOR.calculate(
        ShoppingBasketCalculatorRequest(
            basket=ShoppingBasket(
                items=[item_name]
            ),
            catalogue=catalogues.CATALOGUE_FULL,
            special_offers=special_offers.SPECIAL_OFFERS_FULL
        )
    ) == Receipt(
        sub_total=0,
        discount=0
    )


@pytest.mark.parametrize(
    "items,expected_price",
    [
        (
                [
                    "Bread (Harvest Lcl, White)",
                    "Bread (Harvest Lcl, White)",
                    "Mushrooms (Organic, Portobello)",
                    "Mushrooms (Organic, Portobello)",
                    "Mushrooms (Organic, Portobello)"
                ],
                5.73

        ),
        (
                [
                    "Bread (Harvest Lcl, White)",
                    "Mushrooms (Organic, Portobello)",
                    "Mushrooms (Organic, Portobello)",
                    "Mushrooms (Organic, Portobello)",
                    "Mushrooms (Organic, Portobello)",
                    "Mushrooms (Organic, Portobello)",
                    "Mushrooms (Organic, Portobello)"
                ],
                7.14
        )
    ]
)
def test_shopping_basket_calculator_basket_items_have_no_discount(
        items: [str],
        expected_price: float
) -> None:
    assert _SHOPPING_BASKET_CALCULATOR.calculate(
        ShoppingBasketCalculatorRequest(
            basket=ShoppingBasket(
                items=items
            ),
            catalogue=catalogues.CATALOGUE_FULL,
            special_offers=special_offers.SPECIAL_OFFERS_FULL
        )
    )


@pytest.mark.parametrize(
    "items,expected_price",
    [
        (
                [
                    "Bread (Harvest Lcl, White)",
                    "Mushrooms (Organic, Portobello)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)"
                ],
                3.33

        ),
        (
                [
                    "Bread (Harvest Lcl, White)",
                    "Mushrooms (Organic, Portobello)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)"
                ],
                5.21
        ),
        (
                [
                    "Bread (Harvest Lcl, White)",
                    "Mushrooms (Organic, Portobello)",
                    "Tomatoes (Organic, XS)",
                    "Tomatoes (Organic, XS)",
                    "Tomatoes (Organic, XS)"
                ],
                4.0775

        ),
        (
                [
                    "Bread (Harvest Lcl, White)",
                    "Mushrooms (Organic, Portobello)",
                    "Tomatoes (Organic, XS)",
                    "Tomatoes (Organic, XS)",
                    "Tomatoes (Organic, XS)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)",
                    "Baked Beans (Generic Brand Name)"
                ],
                5.9575

        ),
    ]
)
def test_shopping_basket_calculator_basket_items_have_discount(
        items: [str],
        expected_price: float
) -> None:
    assert _SHOPPING_BASKET_CALCULATOR.calculate(
        ShoppingBasketCalculatorRequest(
            basket=ShoppingBasket(
                items=items
            ),
            catalogue=catalogues.CATALOGUE_FULL,
            special_offers=special_offers.SPECIAL_OFFERS_FULL
        )
    )

import pytest

from app.business.implemetations.special_offer_validator import \
    SpecialOfferValidator


@pytest.mark.parametrize(
    "item_price",
    [
        0.00, -0.01, -0.50
    ]
)
def test_percentage_item_price_lteq_zero(
        item_price: float
) -> None:
    with pytest.raises(ValueError) as _:
        SpecialOfferValidator.ensure_valid_args(
            item_price=item_price,
            item_count=0
        )


@pytest.mark.parametrize(
    "count",
    [
        -1, -3, -5, -8, -13
    ]
)
def test_percentage_count_negative(
        count: int
) -> None:
    with pytest.raises(ValueError) as _:
        SpecialOfferValidator.ensure_valid_args(
            item_price=0.50,
            item_count=count
        )

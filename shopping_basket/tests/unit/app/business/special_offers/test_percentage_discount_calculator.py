import math

import pytest

from app.business.abstractions.special_offers.percentage_discount_calculator import \
    PercentageDiscountCalculator

_PERCENTAGE_DISCOUNT_CALCULATOR = PercentageDiscountCalculator(
    percentage=25
)


@pytest.mark.parametrize(
    "percentage",
    [
        -1, -2, -3, -4, -5,
        101, 102, 103, 104, 105
    ]
)
def test_percentage_discount_percentage_lt_zero_or_gt_100(
        percentage: int
) -> None:
    with pytest.raises(ValueError) as _:
        _ = PercentageDiscountCalculator(
            percentage=percentage
        )


def test_percentage_discount_item_count_zero() -> None:
    assert _PERCENTAGE_DISCOUNT_CALCULATOR.calculate(
        price_and_count=(
            0.50,
            0
        )
    ) == 0.00


@pytest.mark.parametrize(
    "item_price,item_count,expected_discount",
    [
        (0.10, 1, 0.025),
        (4.40, 1, 1.10),
        (3.00, 3, 2.25),
        (7.75, 2, 3.875)
    ]
)
def test_percentage_discount_calculated(
        item_price: float,
        item_count: int,
        expected_discount: float
) -> None:
    assert math.isclose(
        _PERCENTAGE_DISCOUNT_CALCULATOR.calculate(
            price_and_count=(
                item_price,
                item_count
            )
        ),
        expected_discount,
        rel_tol=0.00001
    )

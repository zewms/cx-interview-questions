import pytest

from app.business.abstractions.special_offers.buy_x_get_y_free_calculator import \
    BuyXGetYFreeCalculator

_BUY_X_GET_Y_FREE_CALCULATOR = BuyXGetYFreeCalculator(
    threshold=3
)


@pytest.mark.parametrize(
    "threshold",
    [
        0, -1, -2, -3
    ]
)
def test_buy_x_get_y_free_threshold_lteq_zero(
        threshold: int
) -> None:
    with pytest.raises(ValueError) as _:
        _ = BuyXGetYFreeCalculator(
            threshold=threshold
        )


@pytest.mark.parametrize(
    "count",
    [
        0, 1, 2
    ]
)
def test_buy_x_get_y_free_count_lt_threshold(
        count: int
) -> None:
    assert _BUY_X_GET_Y_FREE_CALCULATOR.calculate(
        price_and_count=(
            0.50,
            count
        )
    ) == 0.00


@pytest.mark.parametrize(
    "item_price,item_count,expected_discount",
    [
        (3.00, 3, 3.00)
    ]
)
def test_buy_x_get_y_free_discount_calculated(
        item_price: float,
        item_count: int,
        expected_discount: float
) -> None:
    calculated = _BUY_X_GET_Y_FREE_CALCULATOR.calculate(
        price_and_count=(
            item_price,
            item_count
        )
    )

    assert calculated == expected_discount

import pytest

from app.models.item import Item


@pytest.mark.parametrize(
    "item_name",
    [
        None,
        "",
        "    "
    ]
)
def test_item_name_is_none_or_empty(
        item_name: str
) -> None:
    with pytest.raises(ValueError) as _:
        _ = Item(
            name=item_name,
            desc="Test description.",
            price=0.5
        )


@pytest.mark.parametrize(
    "item_price",
    [
        0.00,
        -0.01,
        -1
    ]
)
def test_item_price_is_zero_or_negative(
        item_price: float
) -> None:
    with pytest.raises(ValueError) as _:
        _ = Item(
            name="Test Name",
            desc="Test description",
            price=item_price
        )


@pytest.mark.parametrize(
    "item_name,item_price",
    [
        ("Test Name #1", 0.01),
        ("Test Name #2", 0.50),
        ("Test Name #3", 1.49),
        ("Test Name #4", 23.99)
    ]
)
def test_item_attrs_valid(
        item_name: str,
        item_price: float
) -> None:
    item = Item(
        name=item_name,
        desc=None,
        price=item_price
    )

    assert (item.name, item.price) == (item_name, item_price)

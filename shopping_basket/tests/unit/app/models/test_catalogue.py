import pytest

from app.models.catalogue import Catalogue
from app.models.item import Item


@pytest.mark.parametrize(
    "items",
    [
        None,
        []
    ]
)
def test_catalogue_is_none_or_empty(
        items: [Item]
) -> None:
    with pytest.raises(ValueError) as _:
        _ = Catalogue(
            items=items
        )


@pytest.mark.parametrize(
    "items",
    [
        [
            ("Test Name #1", 0.01),
            ("Test Name #2", 0.50)
        ],
        [
            ("Test Name #3", 1.49),
            ("Test Name #4", 23.99)
        ]
    ]
)
def test_catelogue_items_valid(
        items: [(str, float)]
) -> None:
    catalogue_items = Catalogue(
        items=[
            Item(
                name=name,
                desc=None,
                price=price
            )
            for name, price in items
        ]
    ).items()

    assert len(catalogue_items) == len(items)
    for i, item in enumerate(catalogue_items.values()):
        items_name, items_price = items[i]
        assert items_name == item.name
        assert items_price == item.price

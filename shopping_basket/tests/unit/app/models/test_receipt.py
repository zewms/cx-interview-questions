import math

import pytest

from app.models.receipt import Receipt


@pytest.mark.parametrize(
    "sub_total",
    [
        -0.01,
        -0.5,
        -13
    ]
)
def test_receipt_sub_total_is_negative(
        sub_total: float
) -> None:
    with pytest.raises(ValueError) as _:
        _ = Receipt(
            sub_total=sub_total,
            discount=0
        )


@pytest.mark.parametrize(
    "discount",
    [
        -0.01,
        -0.5,
        -13
    ]
)
def test_receipt_discount_is_negative(
        discount: float
) -> None:
    with pytest.raises(ValueError) as _:
        _ = Receipt(
            sub_total=10,
            discount=discount
        )


@pytest.mark.parametrize(
    "sub_total",
    [
        -0.01,
        -0.5,
        -13
    ]
)
def test_receipt_total_is_negative(
        sub_total: float
) -> None:
    with pytest.raises(ValueError) as _:
        _ = Receipt(
            sub_total=sub_total,
            discount=0
        )


@pytest.mark.parametrize(
    "sub_total,discount",
    [
        (0.00, 0.01),
        (0.44, 0.45),
        (0.99, 1.00),
        (1.50, 1.99)
    ]
)
def test_receipt_sub_total_gt_total(
        sub_total: float,
        discount: float
) -> None:
    with pytest.raises(ValueError) as _:
        _ = Receipt(
            sub_total=sub_total,
            discount=discount
        )


@pytest.mark.parametrize(
    "sub_total,discount,expected_total",
    [
        (0.50, 0.15, 0.35),
        (0.50, 0.20, 0.30),
        (1.33, 0.13, 1.20),
        (13.13, 1.13, 12.00)
    ]
)
def test_receipt_total_eq_sub_total_minus_discount(
        sub_total: float,
        discount: float,
        expected_total: float
) -> None:
    assert math.isclose(
        Receipt(
            sub_total=sub_total,
            discount=discount
        ).total,
        expected_total,
        abs_tol=0.00001
    )

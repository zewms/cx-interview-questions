from typing import Final

ITEMS_FULL: Final = {
    "Baked Beans (Generic Brand Name)",
    "Tomatoes (Organic, XS)",
    "Bread (Harvest Lcl, White)",
    "Mushrooms (Organic, Portobello)"
}

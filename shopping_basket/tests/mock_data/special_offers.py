from typing import Final

from app.business.implemetations.special_offers.baked_beans_buy_two_get_one_free_calculator import \
    BakedBeansBuyTwoGetOneFreeCalculator
from app.business.implemetations.special_offers.tomatoes_twenty_five_percent_discount import \
    TomatoesTwentyFivePercentageDiscount
from app.models.special_offers import SpecialOffers

SPECIAL_OFFERS_FULL: Final = SpecialOffers(
    items={
        "Baked Beans (Generic Brand Name)": BakedBeansBuyTwoGetOneFreeCalculator(),
        "Tomatoes (Organic, XS)": TomatoesTwentyFivePercentageDiscount()
    }
)

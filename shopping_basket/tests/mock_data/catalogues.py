from typing import Final

from app.models.catalogue import Catalogue
from app.models.item import Item

CATALOGUE_FULL: Final = Catalogue(
    items=[
        Item(
            name="Baked Beans (Generic Brand Name)",
            desc="Juicy... :D",
            price=0.47
        ),
        Item(
            name="Tomatoes (Organic, XS)",
            desc="Roast 'em, eep!",
            price=0.75
        ),
        Item(
            name="Bread (Harvest Lcl, White)",
            desc="Switch to wholewheat! ^______^",
            price=1.44
        ),
        Item(
            name="Mushrooms (Organic, Portobello)",
            desc="Mhm, tasty! :)",
            price=0.95
        )
    ])

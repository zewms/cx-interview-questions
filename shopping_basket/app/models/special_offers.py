import dataclasses as dc
from typing import Dict

from app.business.abstractions.calculator import Calculator


@dc.dataclass(frozen=True)
class SpecialOffers:
    items: Dict[str, Calculator]

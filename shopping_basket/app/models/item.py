import dataclasses as dc
from typing import Optional


@dc.dataclass(frozen=True)
class Item:
    name: str
    desc: Optional[str]
    price: float

    def __post_init__(self):
        if not self.name or self.name.isspace():
            raise ValueError(
                "Name was null or empty."
            )

        if self.price <= 0.00:
            raise ValueError(
                "Price was negative or zero."
            )

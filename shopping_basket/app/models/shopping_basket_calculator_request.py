import dataclasses as dc
from typing import Optional

from app.business.abstractions.basket import Basket
from app.models.catalogue import Catalogue
from app.models.special_offers import SpecialOffers


@dc.dataclass(frozen=True)
class ShoppingBasketCalculatorRequest:
    basket: Basket
    catalogue: Catalogue
    special_offers: Optional[SpecialOffers]

import dataclasses as dc
from typing import Dict

from app.models.item import Item


@dc.dataclass()
class Catalogue:
    def __init__(
            self,
            items: [Item]
    ) -> None:
        if not items:
            raise ValueError(
                "Catalogue was null or empty."
            )

        self._items = dict([
            (item.name, item)
            for item in items
        ])

    def items(self) -> Dict[str, Item]:
        return self._items

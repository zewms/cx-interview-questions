import dataclasses as dc


@dc.dataclass
class Receipt:
    sub_total: float
    discount: float
    total: dc.field(init=False) = 0.00

    def __post_init__(self):
        if self.sub_total < 0:
            raise ValueError(
                "Subtotal invalid: was negative "
                f"(subtotal: {self.sub_total})."
            )

        if self.discount < 0:
            raise ValueError(
                "Discount invalid: was negative "
                f"(discount: {self.discount})."
            )

        if self.total < 0:
            raise ValueError(
                "Total invalid: was negative "
                f"(total: {self.total})."
            )

        if self.discount > self.sub_total:
            raise ValueError(
                "Discount invalid: was greater than subtotal "
                f"(discount: {self.discount}, subtotal: {self.sub_total})."
            )

        self.total = self.sub_total - self.discount

    def __str__(self):
        return \
            "\nORDER RECEIPT\n-----\n" \
            f"Subtotal: \t{self.sub_total:.2f}\n" \
            f"Discount: \t{self.discount:.2f}\n" \
            f"Total: \t\t{self.total:.2f}\n"

    def __repr__(self):
        return self.__str__()

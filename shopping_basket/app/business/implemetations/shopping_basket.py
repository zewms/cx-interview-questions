from typing import Tuple

from app.business.abstractions.basket import Basket
from app.models.item import Item


class ShoppingBasket(Basket):
    def __init__(
            self,
            items: [str] = None
    ):
        self._items = {}
        [self.add(item) for item in items] if items else None

    def items(self) -> [Tuple[str, int]]:
        return list(self._items.items())

    def add(
            self,
            item_name: str
    ) -> Tuple[str, int]:
        if not item_name or item_name.isspace():
            raise ValueError(
                "Invalid item: was None."
            )

        self._items[item_name] = self._items.get(
            item_name,
            0
        ) + 1

        return item_name, self._items[item_name]

    def remove(
            self,
            item_name: str
    ) -> Tuple[str, int]:
        if item_name not in self._items:
            raise ValueError(
                "Invalid item name: basket contains no items with "
                "the provided name "
                f"(was: {item_name if item_name else 'None'})."
            )

        return item_name, self._items.pop(item_name)

    def clear(self) -> [Tuple[Item, int]]:
        self._items.clear()

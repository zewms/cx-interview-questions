from typing import Tuple

from app.business.abstractions.special_offers.percentage_discount_calculator import \
    PercentageDiscountCalculator


class TomatoesTwentyFivePercentageDiscount(PercentageDiscountCalculator):
    """In practicality, this wouldn't need to exist, we could just load all
    'BuyXGetYFreeCalculator' from a json file, that specifies thresholds,
    names, etc.
    """

    def __init__(
            self
    ):
        super().__init__(
            percentage=25
        )

    def calculate(
            self,
            price_and_count: Tuple[float, int]
    ) -> float:
        return super().calculate(
            price_and_count=price_and_count
        )

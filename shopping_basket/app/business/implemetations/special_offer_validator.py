class SpecialOfferValidator:
    @staticmethod
    def ensure_valid_args(
            item_price: float,
            item_count: int
    ) -> None:
        """This shouldn't be static, but should be defined as an
        implementation of an interface, to be injected into the
        relevant special offer calculators.
        """

        if item_price <= 0:
            raise ValueError(
                f"Invalid item price: was negative or zero (price: {item_price})."
            )

        if item_count < 0:
            raise ValueError(
                f"Invalid item count: was negative (count: {item_count})."
            )

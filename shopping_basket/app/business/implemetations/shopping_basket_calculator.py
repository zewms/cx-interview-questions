from typing import Dict

from app.business.abstractions.calculator import Calculator
from app.models.item import Item
from app.models.receipt import Receipt
from app.models.shopping_basket_calculator_request import \
    ShoppingBasketCalculatorRequest


class ShoppingBasketCalculator(Calculator):
    def calculate(
            self,
            request: ShoppingBasketCalculatorRequest
    ) -> Receipt:
        if not request.basket.items():
            return Receipt(
                sub_total=0,
                discount=0
            )

        special_offers = (
            request.special_offers.items
            if request.special_offers
            else None
        )

        sub_total = discount = 0
        catalogue = request.catalogue.items()
        for item_name, item_count in request.basket.items():
            basket_item = catalogue.get(item_name)

            if basket_item is None:
                """Originally, this threw an error, but after re-reading
                requirements, specifically where it is mentioned that ownership
                of the relevant functionality may be spread amongst teams,
                I have now just ignored it.

                In a real world scenario, I'd likely build something into
                the configuration to determine how the occurence of this
                behaviour should be handled, and proceed to handle it as is
                defined there. 
                """
                continue

            sub_total += basket_item.price * item_count
            discount += self._get_discount(
                special_offers=special_offers,
                item_obj=basket_item,
                total_count=item_count
            )

        return Receipt(
            sub_total=sub_total,
            discount=discount
        )

    @staticmethod
    def _get_discount(
            special_offers: Dict[str, Calculator],
            item_obj: Item,
            total_count: int
    ) -> float:
        return (
            special_offers.get(item_obj.name).calculate(
                (item_obj.price, total_count)
            )
            if special_offers and item_obj.name in special_offers
            else 0
        )

import abc


class Calculator:
    @abc.abstractmethod
    def calculate(
            self,
            to_calculate: any
    ) -> any:
        pass

import abc
import math
from typing import Tuple

from app.business.abstractions.calculator import Calculator
from app.business.implemetations.special_offer_validator import \
    SpecialOfferValidator


class BuyXGetYFreeCalculator(Calculator):
    def __init__(
            self,
            threshold: int
    ):
        if threshold <= 0:
            raise ValueError(
                "Invalid percentage, was zero or negative "
                f"(percentage: {threshold})."
            )

        self._threshold = threshold

    @abc.abstractmethod
    def calculate(
            self,
            price_and_count: Tuple[float, int]
    ) -> float:
        item_price, item_count = price_and_count

        # This should not be static, and should instead be injected into
        # __init__.
        SpecialOfferValidator.ensure_valid_args(
            item_price=item_price,
            item_count=item_count
        )

        if item_count < self._threshold:
            return 0

        return math.floor(item_count / self._threshold) * item_price

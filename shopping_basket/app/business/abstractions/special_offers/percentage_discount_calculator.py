import abc
from typing import Tuple

from app.business.abstractions.calculator import Calculator
from app.business.implemetations.special_offer_validator import \
    SpecialOfferValidator


class PercentageDiscountCalculator(Calculator):
    def __init__(
            self,
            percentage: float
    ):
        if percentage < 0 or percentage > 100:
            raise ValueError(
                "Invalid percentage, was less than zero or greater than 100"
                f"(percentage: {percentage})."
            )

        self._percentage = percentage

    @abc.abstractmethod
    def calculate(
            self,
            price_and_count: Tuple[float, int]
    ) -> float:
        item_price, item_count = price_and_count

        # This should not be static, and should instead be injected into
        # __init__.
        SpecialOfferValidator.ensure_valid_args(
            item_price=item_price,
            item_count=item_count
        )

        if item_count == 0:
            return 0

        return ((item_price * item_count) / 100) * self._percentage

import abc
from typing import Tuple

from app.models.item import Item


class Basket(abc.ABC):
    @abc.abstractmethod
    def items(self) -> [Tuple[Item, int]]:
        pass

    @abc.abstractmethod
    def add(
            self,
            item: any
    ) -> None:
        pass

    @abc.abstractmethod
    def remove(
            self,
            item_name: any
    ) -> any:
        pass

    @abc.abstractmethod
    def clear(self):
        pass
